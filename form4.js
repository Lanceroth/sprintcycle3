function validar_nombre(string) {
    var nombre = document.getElementById("campoNombre").value;

    if(nombre == ""){
        window.alert("El nombre es obligatorio");
        return false;
    }
    else if(nombre.length <4 || nombre.length > 30){
        window.alert("El nombre de contener entre 4 y 30 caracteres");
        document.getElementById("campoNombre").value= "";
        document.getElementById("campoNombre").focus();
        return false;
    }
    else if (!/^[a-zA-Z]*$/g.test(nombre)) {
        window.alert("El nombre no puede contener números ni caracteres especiales");
        document.getElementById("campoNombre").value= "";
        document.getElementById("campoNombre").focus();
        return false;
    }
}

function validar_contrasenas(string) {
    var pass1 = ((document.getElementById("contraseña_usuario")||{}).value)||"";
    var pass2 = ((document.getElementById("campoConfirmarContrasena")||{}).value)||""
    validar= new Boolean(true)
    expresion= /\s/;

    if (pass1.length == 0){
        window.alert("La contraseña es obligatoria");
        pass1=pass1.value;
        return false;
    }
    else if(expresion.test(pass1)){
        window.alert("La contraseña no puede tener espacios en blanco");
        document.getElementById("contraseña_usuario").pass1;
        document.getElementById("contraseña_usuario").focus();
        return false;
    }
    else if(pass2.length == 0 || pass2==null){
        window.alert("Es necesario que confirme su contraseña");
        return false;
    }
    else if ((pass1) !== (pass2)){
        window.alert("las contraseñas no coinciden");
        document.getElementById("contraseña_usuario").pass1;
        document.getElementById("campoConfirmarContrasena").pass2;
        document.getElementById("contraseña_usuario").focus();
        document.getElementById("campoConfirmarContrasena").focus();
        return false;
    }
    return validar
}

/*  /^\s+$/.test  :Expresion regular verifica si digita uno o varios espacios en el campo */

module.exports.validar_nombre = validar_nombre
module.exports.validar_contrasenas = validar_contrasenas;
