# SprintCycle3

Nuestro primer proyecto en GIT

# Create a new repository


- git config --global user.name "name"
- git config --global user.email "email"
- git config --global --list
- DIR file>git init
- git clone https://gitlab.com/Lanceroth/sprintcycle3.git
- git status
- cd sprintcycle3
- git switch -c main // git branch -M main
- git add README.md
- git pull origin main --allow-unrelated-histories
- git commit -m "add README"
- git push -u origin main

# Push an existing folder

- cd existing_folder
- git init --initial-branch=main
- git remote add origin https://gitlab.com/Lanceroth/sprintcycle3.git
- git add .
- git commit -m "Initial commit"
- git push -u origin main


# Push an existing Git repository

- cd existing_repo
- git remote rename origin old-origin
- git remote add origin https://gitlab.com/Lanceroth/sprintcycle3.git
- git push -u origin --all
- git push -u origin --tags

